import os
import sys
import time

from subprocess import Popen, PIPE, STDOUT

# Recurse for zip files
TEMP_DIR = '/tmp/phonebaks'

AB_JAR_PATH = os.path.join(
    os.path.dirname(__file__),
    'abextractor/android-backup-extractor-20160710-bin/abe.jar'
)

os.path.exists(TEMP_DIR) or os.makedirs(TEMP_DIR) # Create temp directory

def progress(count, total, suffix=''):
    """Progress indicator.

    Source
        https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
        https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
    """
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  # As suggested by Rom Ruben


def unzip_files(zips_dir):
    """Unzip each file to their own directory

    Grabs all files ending in *.zip, unzips them, and
    dumps them to `/var/tmp`

    Args
        zips_dir: string used to specify directory containing backups

    Returns
        None

    Shell commands
        `unzip <filename>.zip -d <TEMP_DIR>`
    """
    zips = [fname for fname in os.listdir(zips_dir) if ".zip" in fname] # Find all of the zip files

    print '%s(s) .zip files found' % len(zips)

    for fname in zips:
        print fname

    for idx, z in enumerate(zips):

        task = Popen(
            ['unzip', z , '-d', '{0}/{1}'.format(TEMP_DIR, z[:-4])],
            stdout=PIPE, stdin=PIPE, stderr=PIPE, cwd=zips_dir
        )

        task.communicate(input='y \n') # Automatically choose default answers

        progress(idx+1, len(zips), 'Unzipping %s' % z)


def extract_ab(source_dir):
    """Find all *.ab files and extract their contents.

    Args:
        source_dir: str, the location of *.ab files

    Returns:
        None

    NOTE: This function assumes a valid java installation and
          abe.jar is present on host machine

    Uses abextractor to unpack the contents from *.ab files
    and outputs the resulting file in the same directory
    with the following filename:

        source_dir/<directory .ab found>_<name of .ab file>.tar

    Shell commands:
        `java -jar <path_to_abe.js> unpack <path_to_*.ab> <output_path>`
    """
    for root, dirs, files in os.walk(source_dir):
        ab_files = [fname for fname in files if ".ab" in fname]

        for idx, bak in enumerate(ab_files):
            abs_path = os.path.join(root, bak)
            output_name = "{0}.tar".format(
                root.replace(source_dir, '').replace('/', '_')
            ) # Strip off the source_dir path

            abs_out_path = "{0}/{1}".format(source_dir, output_name) # Put the file back in the source_dir

            task = Popen(
                ["java", "-jar", AB_JAR_PATH, "unpack", abs_path, abs_out_path],
                stdout=PIPE, stdin=PIPE, stderr=PIPE
            )

            task.communicate()


def untar_baks(target_dir):
    """Untar files within specified directory.

    Args:
        target_dir: str, the location of tar files

    Shell commands:
        `tar -xf <tar_file> -C <target_dir>/<orginal_tar_name>`
    """
    os.path.exists(target_dir) or os.makedirs(target_dir)
    tar_files = [f for f in os.listdir(TEMP_DIR) if '.tar' in f]

    print '%s(s) .tar files found' % len(tar_files)

    for fname in tar_files:
        print fname

    for idx, tar in enumerate(tar_files):

        output_path = "{0}/{1}".format(target_dir, tar.replace('.tar', ''))

        os.path.exists(output_path) or os.makedirs(output_path)

        task = Popen(
            ['tar', '-xvf', tar, '-C', output_path],
            stdout=PIPE, stdin=PIPE, stderr=PIPE, cwd=TEMP_DIR
        )

        task.communicate()

        progress(idx+1, len(tar_files), 'Decompressing file %s' % tar)

    print 'Successfully extracted %s android backup(s) to %s' % (len(tar_files), target_dir)


def clean_up():
    """Clean up and remove contents placed in /tmp directory
    """
    task = Popen(
        ['rm', '-rf', TEMP_DIR],
        stdout=PIPE, stdin=PIPE, stderr=PIPE
    )

    task.communicate()
    progress(1, 1, 'Cleaning up')

