#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='android-extractor',
    version='0.1',
    description='Android extraction tool',
    url='https://bitbucket.org/nathandouglas/android-extractor',
    author='Nathan Douglas',
    author_email='nathan.a.douglas@gmail.com',
    license='MIT',
    packages=find_packages(),
    zip_safe=False,
    scripts=['bin/abextract']
)
