# Requirements
* Java runtime environment (>= jdk1.7)
* Python (2.7.13)

# Install
* `cd <install_directory>`
* `pip install .`

# Usage
* `abextract -s </source/to/backups> -d </output/destination>`
* `abextract -s </source/to/zips> -d </output/destination> -u`
* `abextract -h`
